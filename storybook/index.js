// storybook/config.js
import { getStorybookUI, configure } from '@storybook/react-native';

import './rn-addons';

/// three lines bellow for testing
{/*
  
import registerRequireContextHook from 'babel-plugin-require-context-hook/register';
registerRequireContextHook();
const req = global.__requireContext(__dirname, '../components/stories', true, /.stories.js$/)

*/}

//this line only for display
const req = require.context('../components/stories/', true, /stories\.js$/)

// import stories
configure(() => {
  req.keys().forEach(req);
}, module);

const StorybookUIRoot = getStorybookUI({
  asyncStorage:null,
  port: 7007, host: 'localhost'
});

export default StorybookUIRoot;