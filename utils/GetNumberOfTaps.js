const getNumberOfTaps = menu => {
  let numberOfTaps = 0
  if (menu.sections) {
    menu.sections.forEach((section, index) => {
      numberOfTaps += section.items.length
    })
  }

  return numberOfTaps
}

export default getNumberOfTaps
