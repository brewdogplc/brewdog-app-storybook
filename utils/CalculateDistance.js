const CalculateDistance = ({ pointA, pointB, unit }) => {
  const p = 0.017453292519943295 // Math.PI / 180
  const c = Math.cos
  const a =
    0.5 -
    c((pointB.latitude - pointA.latitude) * p) / 2 +
    (c(pointA.latitude * p) *
      c(pointB.latitude * p) *
      (1 - c((pointB.longitude - pointA.longitude) * p))) /
      2
  return unit === 'km'
    ? Math.round(12742 * Math.asin(Math.sqrt(a)) * 10) / 10
    : Math.round(12742 * Math.asin(Math.sqrt(a)) * 0.62137 * 10) / 10 // 2 * R; R = 6371 km
}

export default CalculateDistance
