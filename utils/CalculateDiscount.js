import Moment from 'moment'

export const isDaytimeDiscountActive = () => {
  var format = 'hh:mm:ss'
  const now = Moment()
  //if its the weekend
  if (now.day() === 6 || now.day() === 0) {
    if (now.isBetween(Moment('07:00:00', format), Moment('12:30:00', format))) {
      return true
    }
  } else {
    if (now.isBetween(Moment('07:00:00', format), Moment('17:00:00', format))) {
      return true
    }
  }
  return false
}

export const calculateOnlineDiscount = (user, prop) => {
  let discount = null
  //if staff
  if (user.group_id === 11) return 30

  user.custom_attributes.map(attribute => {
    if (attribute.attribute_code === prop) {
      switch (attribute.value.toLowerCase()) {
        case 'efp_10': {
          discount = 10
          break
        }
        case 'efp_15': {
          discount = 15
          break
        }
        case 'efp_20': {
          discount = 20
          break
        }
        case 'bond_20': {
          discount = 20
          break
        }
        default: {
          break
        }
      }
    }
  })

  return discount
}

export const calculateBarDiscount = barDiscountCardId => {
  let discount = null

  if (barDiscountCardId) {
    switch (barDiscountCardId.substring(0, 5)) {
      //BB 5 years
      case '53768': {
        //daytime
        discount = 10
        break
      }
      //BB 5 lifetime
      case '53769': {
        //daytime
        discount = 10
        break
      }
      //beer visa
      case '53796': {
        break
      }
      //BrewDog Bond
      case '53746': {
        discount = 10
        break
      }
      //BrewDog Bond 2
      case '53787': {
        discount = 10
        break
      }
      //BA 10% discount
      case '53883': {
        discount = 10
        break
      }
      //EFP < 20
      case '53795': {
        //daytime
        discount = 5
        break
      }
      //EFP >=20
      case '53736': {
        //daytime
        discount = 10
        break
      }
      //EFP >=20 shares
      case '53629': {
        //daytime
        discount = 10
        break
      }
      //EFP promo
      case '53743': {
        discount = 10
        break
      }
      //staff
      case '53637': {
        discount = 25
        break
      }
      //trade
      case '53761': {
        discount = 10
        break
      }
      default: {
        break
      }
    }
  }

  return discount
}
