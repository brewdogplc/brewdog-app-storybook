import Moment from 'moment'

const isOpen = (open, close, timezone) => {
  const mOpen = Moment(open, 'HH:mm:ss') // TODO Timezone Moment(open, 'HH:mm:ss', timezone)
  const mClose = Moment(close, 'HH:mm:ss')
  if (mClose.isBefore(mOpen)) {
    if (Moment().isBetween(mOpen, mClose.add(1, 'days'))) return true
    return false
  }
  if (Moment().isBetween(mOpen, mClose)) return true
  return false
}

export default isOpen
