const GetCustomUserAttribute = (attributeName, user) => {
  let attributeValue = null
  if (user && user.custom_attributes) {
    user.custom_attributes.map(attribute => {
      const { attribute_code, value } = attribute
      if (attribute_code === attributeName) {
        attributeValue = value
      }
    })
    return attributeValue
  } else {
    return null
  }
}

export default GetCustomUserAttribute
