import axios from 'axios'

const CONFIG_URI = 'https://brewdogmedia.s3.eu-west-2.amazonaws.com/docs/brewdog_app_config.json'
const VENUES_URI = 'https://s3.eu-west-2.amazonaws.com/brewdogmedia/docs/Venues.json'
//for dev only
//const VENUES_URI = 'https://s3.eu-west-2.amazonaws.com/brewdogmedia/docs/Venues_test.json'
const UNTAPPD_URI = 'https://business.untappd.com/api/v1'
const MAGENTO_URI = 'https://www.brewdog.com/index.php/rest/V1'
const MAGENTO_ADMIN_TOKEN = 'y99a5p6dhqspwr51h5z9r6h7t0zuaw5x'
const MTC_URI = 'https://efp.brewdog.com/api/v0.2'
const MTC_AUTH_TOKEN =
  'Bearer meuhWuCFrDdDvE93mzqNx5mwrcydt3F7Wht9uWqF7RQYAm7bL2K3DFPtK9GZMGghCrpzjSgCt6bKjRkv6VFXvbagU38DC9ZdBd7Xj4PBaP6d9Yxdn2Q2Ep8dRYU6DLCU, MemberID 1'

const BrewDogService = {
  loadConfig: () => {
    return axios.get(CONFIG_URI, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: 0
      }
    })
  },
  getVenues: () => {
    return axios.get(VENUES_URI, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: 0
      }
    })
  },
  getMenu: id => {
    return axios.get(`${UNTAPPD_URI}/menus/${id}?full=true`, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: 0
      },
      auth: {
        username: 'app@brewdog.com',
        password: '1v7S88TbCipNAdrs8ekx'
      }
    })
  },
  getUser: id => {
    return axios.get(`${MAGENTO_URI}/customers/${id}`, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: 0,
        Authorization: 'bearer ' + MAGENTO_ADMIN_TOKEN
      }
    })
  },
  getUserWithToken: token => {
    return axios.get(`${MAGENTO_URI}/customers/me`, {
      headers: {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: 0,
        Authorization: 'bearer ' + token
      }
    })
  },
  getUserWithUsername: username => {
    return axios.get(
      `${MAGENTO_URI}/customers/search?searchCriteria[filterGroups][0][filters][0][field]=email&searchCriteria[filterGroups][0][filters][0][value]=${username}&searchCriteria[filterGroups][0][filters][0][conditionType]=equals`,
      {
        headers: {
          'Cache-Control': 'no-cache, no-store, must-revalidate',
          Pragma: 'no-cache',
          Expires: 0,
          Authorization: 'bearer ' + MAGENTO_ADMIN_TOKEN
        }
      }
    )
  },
  loginUser: (username, password) => {
    return axios.post(`${MAGENTO_URI}/integration/customer/token`, {
      username: username,
      password: password
    })
  },
  getUserBarDiscountCardId: mtcId => {
    return axios.get(`${MTC_URI}/member/bardiscountcard/${mtcId}`, {
      headers: {
        Authorization: MTC_AUTH_TOKEN
      }
    })
  },
  getUserBondCardId: mtcId => {
    return axios.get(`${MTC_URI}/member/bondcard/${mtcId}`, {
      headers: {
        Authorization: MTC_AUTH_TOKEN
      }
    })
  },
  getUserUSABarDiscountCardId: mtcId => {
    return axios.get(`${MTC_URI}/member/bardiscountcardusa/${mtcId}`, {
      headers: {
        Authorization: MTC_AUTH_TOKEN
      }
    })
  },
  setMyLocal: (user, myLocalId, myLocalResetDate) => {
    return axios.put(
      `${MAGENTO_URI}/customers/${user.id}`,
      {
        customer: {
          id: user.id,
          group_id: user.group_id,
          email: user.email,
          firstname: user.firstname,
          lastname: user.lastname,
          store_id: user.store_id,
          website_id: user.website_id,
          custom_attributes: [
            {
              attribute_code: 'my_local_id',
              value: myLocalId
            },
            {
              attribute_code: 'my_local_reset_date',
              value: myLocalResetDate
            }
          ]
        }
      },
      {
        headers: {
          Authorization: 'bearer ' + MAGENTO_ADMIN_TOKEN
        }
      }
    )
  },
  applyMyLocal: (legacyId, alohaId) => {
    return axios.post(
      `https://9k91w0hthe.execute-api.eu-west-2.amazonaws.com/dev/${legacyId}/my-local`,
      { alohaId },
      {
        headers: {
          'x-api-key': '7ngix2UQ8U1CAnKGkjREsawOh1JG6Bt77zAUxrnQ'
        }
      }
    )
  }
}

export default BrewDogService
