import axios from 'axios';

const PUNK_API_URL = 'http://localhost:3000';
const PUNK_API_APP_TOKEN = 'brewdog';

//get open bars
export async function getVenues() {
  return axios.get(`${PUNK_API_URL}/venues?open=true`, {
    headers: {
      Authorization: `Bearer ${PUNK_API_APP_TOKEN}`,
      'Cache-Control': 'no-cache',
    },
  });
}

//get untappd menu
export async function getBarMenu(menuId) {
  return axios.get(`${PUNK_API_URL}/untappd/menu/${menuId}`, {
    headers: {
      Authorization: `Bearer ${PUNK_API_APP_TOKEN}`,
      'Cache-Control': 'no-cache',
    },
  });
}

//authenticate with username and password
export async function authenticate(username, password) {
  return axios.post(
    `${PUNK_API_URL}/users/auth`,
    {
      username,
      password,
    },
    {
      headers: {
        Authorization: `Bearer ${PUNK_API_APP_TOKEN}`,
        'Cache-Control': 'no-cache',
      },
    }
  );
}

//refresh authentication with token
export async function refreshAuthentication(token) {
  return axios.get(`${PUNK_API_URL}/users/auth/refresh`, {
    headers: {
      Authorization: `Bearer ${token}`,
      'Cache-Control': 'no-cache',
    },
  });
}

//get user
export async function getUser(userId, token) {
  return axios.get(`${PUNK_API_URL}/users/${userId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
      'Cache-Control': 'no-cache',
    },
  });
}
