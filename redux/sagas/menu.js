import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';

import { types } from '../ducks/menu';
import { getBarMenu } from '../../services/PunkAPI';

function* getMenu(action) {
  const { id, sortBy } = action.payload;
  try {
    const { data } = yield call(getBarMenu, id);

    _sortMenu(data, sortBy);
    data.sections.map((section) => {
      section.data = section.items;
    });

    data.numberOfBeers = _getNumberOfBeers(data);

    yield put({ type: types.GET_MENU_SUCCESS, data });
  } catch (error) {
    yield put({ type: types.GET_MENU_FAILURE, error });
  }
}

const _sortMenu = (data, sortBy) => {
  if (sortBy) {
    switch (sortBy.toUpperCase()) {
      case 'ABV ASCENDING':
        _.map(data.sections, (section) => {
          section.data = section.items.sort((a, b) => a.abv - b.abv);
        });
        break;
      case 'ABV DESCENDING':
        data.sections.map((section) => {
          section.data = section.items.sort((a, b) => b.abv - a.abv);
        });
        break;
      case 'RATING ASCENDING':
        data.sections.map((section) => {
          section.data = section.items.sort((a, b) =>
            a.rating > b.rating ? 1 : -1
          );
        });
        break;
      case 'RATING DESCENDING':
        data.sections.map((section) => {
          section.data = section.items.sort((a, b) =>
            a.rating < b.rating ? 1 : -1
          );
        });
        break;
    }
  }

  return data;
};

const _getNumberOfBeers = (menu) => {
  let count = 0;
  if (menu.sections) {
    menu.sections.forEach((section) => {
      if (section.items) count += section.items.length;
    });
  }

  return count;
};

export const menuSagas = [takeLatest(types.GET_MENU_REQUEST, getMenu)];
