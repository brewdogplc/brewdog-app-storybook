import { call, put, takeLatest } from 'redux-saga/effects';
import AsyncStorage from '@react-native-community/async-storage';

import { authenticate, refreshAuthentication } from '../../services/PunkAPI';
import { types } from '../ducks/auth';

function* login(action) {
  const { username, password } = action.payload;

  //regular user login
  if (username && password) {
    try {
      //authenticate user
      const { data } = yield call(authenticate, username, password);

      //set token in async storage
      yield call(AsyncStorage.setItem, 'token', data.token);

      yield put({
        type: types.LOGIN_SUCCESS,
        userId: data.user.id,
      });
    } catch (error) {
      yield put({ type: types.LOGIN_FAILURE, error });
    }
  } else {
    yield put({
      type: types.LOGIN_FAILURE,
      error: 'Must enter a username and password',
    });
  }
}

function* refreshToken() {
  //get token from storage
  const token = yield call(AsyncStorage.getItem, 'token');

  //refresh token
  if (token) {
    try {
      const { data } = yield call(refreshAuthentication, token);

      //set token in async storage
      yield call(AsyncStorage.setItem, 'token', data.token);

      //set user in redux store
      yield put({
        type: types.REFRESH_TOKEN_SUCCESS,
        userId: data.user.id,
      });
    } catch (error) {
      //remove token from async storage
      yield call(AsyncStorage.removeItem, 'token');
      yield put({ type: types.REFRESH_TOKEN_FAILURE, error });
    }
  } else {
    yield put({
      type: types.REFRESH_TOKEN_FAILURE,
      error: 'No token',
    });
  }
}

export const authSagas = [
  takeLatest(types.LOGIN_REQUEST, login),
  takeLatest(types.REFRESH_TOKEN_REQUEST, refreshToken),
];
