import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import Moment from 'moment';

import { types } from '../ducks/bars';
import { getVenues } from '../../services/PunkAPI';

function* getBars(action) {
  const { location } = action.payload;

  try {
    const { data } = yield call(getVenues);

    yield put({
      type: types.GET_BARS_SUCCESS,
      data: _processVenues(data, location),
    });
  } catch (error) {
    yield put({ type: types.GET_BARS_FAILURE, error });
  }
}

const _processVenues = (venues, location) => {
  return _.map(venues, (venue) => {
    return {
      ...venue,
      openStatus: _getOpeningStatus(venue),
      distance: location
        ? _calculateDistance(location, {
            latitude: venue.location.lat,
            longitude: venue.location.lon,
          })
        : null,
    };
  });
};

const _calculateDistance = (pointA, pointB) => {
  const p = 0.017453292519943295; // Math.PI / 180
  const c = Math.cos;
  const a =
    0.5 -
    c((pointB.latitude - pointA.latitude) * p) / 2 +
    (c(pointA.latitude * p) *
      c(pointB.latitude * p) *
      (1 - c((pointB.longitude - pointA.longitude) * p))) /
      2;

  return Math.round(12742 * Math.asin(Math.sqrt(a)) * 0.62137 * 10) / 10; // 2 * R; R = 6371 km
};

const _getOpeningStatus = (venue) => {
  if (venue.openingTimes) {
    //get the opening hours for today
    const openingTimesToday = _.find(venue.openingTimes, {
      day: Moment()
        .subtract(2, 'hours') //subtract 2 hours as our bars stay open to 2am
        .format('dddd')
        .toLowerCase(),
    });

    if (openingTimesToday.closed) {
      return 'Closed';
    }

    if (openingTimesToday.always_open) {
      return 'Always Open';
    }

    return `Open until ${Moment(openingTimesToday.close_at).format('h:mma')}`;
  }

  return null;
};

export const barsSagas = [takeLatest(types.GET_BARS_REQUEST, getBars)];
