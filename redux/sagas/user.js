import { select, call, put, takeLatest } from 'redux-saga/effects';
import AsyncStorage from '@react-native-community/async-storage';

import { getUser as getPunkUser } from '../../services/PunkAPI';
import { types } from '../ducks/user';
import { types as authTypes } from '../ducks/auth';

export const getUserId = (state) => state.auth.userId;

function* getUser() {
  try {
    //get token from async
    const token = yield call(AsyncStorage.getItem, 'token');

    //get userID from auth
    const userId = yield select(getUserId);

    if (!userId || !token) {
      yield put({
        type: types.GET_USER_FAILURE,
        error: 'no userId or token found',
      });
    } else {
      //get user
      const { data } = yield call(getPunkUser, userId, token);
      yield put({
        type: types.GET_USER_SUCCESS,
        data,
      });
    }
  } catch (error) {
    yield put({
      type: types.GET_USER_FAILURE,
      error,
    });
  }
}

function* setMyLocal(action) {}

function* logout() {
  console.log('logout');
  try {
    //remove token from async storage
    yield call(AsyncStorage.removeItem, 'token');

    //clear auth redux store
    yield put({
      type: authTypes.LOGOUT,
    });

    //clear user redux store
    yield put({
      type: types.LOGOUT_SUCCESS,
    });
  } catch (error) {
    yield put({
      type: types.LOGOUT_FAILURE,
      error,
    });
  }
}

export const userSagas = [
  takeLatest(types.GET_USER_REQUEST, getUser),
  takeLatest(types.SET_USER_MY_LOCAL_REQUEST, setMyLocal),
  takeLatest(types.LOGOUT_REQUEST, logout),
];
