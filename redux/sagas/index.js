import { all } from 'redux-saga/effects';
import { barsSagas } from './bars';
import { menuSagas } from './menu';
import { userSagas } from './user';
import { authSagas } from './auth';

export default function* rootSaga() {
  yield all([...barsSagas, ...menuSagas, ...userSagas, ...authSagas]);
}
