export const types = {
  GET_USER_REQUEST: 'GET_USER_REQUEST',
  GET_USER_SUCCESS: 'GET_USER_SUCCESS',
  GET_USER_FAILURE: 'GET_USER_FAILURE',
  SET_USER_MY_LOCAL_REQUEST: 'SET_USER_MY_LOCAL_REQUEST',
  SET_USER_MY_LOCAL_SUCCESS: 'SET_USER_MY_LOCAL_SUCCESS',
  SET_USER_MY_LOCAL_FAILURE: 'SET_USER_MY_LOCAL_FAILURE',
  LOGOUT_REQUEST: 'LOGOUT_REQUEST',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
  LOGOUT_FAILURE: 'LOGOUT_FAILURE',
};

export const initialState = {
  data: null,
  loading: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_USER_REQUEST:
      return { ...state, loading: true, error: null };
    case types.GET_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
      };
    case types.GET_USER_FAILURE:
      return { ...state, loading: false, error: action.error };
    case types.SET_USER_MY_LOCAL_REQUEST:
      return { ...state, loading: true, error: null };
    case types.SET_USER_MY_LOCAL_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
      };
    case types.SET_USER_MY_LOCAL_FAILURE:
      return {
        loading: false,
        error: action.error,
      };
    case types.LOGOUT_REQUEST:
      return { ...state, loading: true, error: null };
    case types.LOGOUT_SUCCESS:
      return {
        ...initialState,
      };
    case types.LOGOUT_FAILURE:
      return { ...state, loading: false, error: action.error };
    default:
      return state;
  }
};

export const actions = {
  getUser: () => ({ type: types.GET_USER_REQUEST }),
  setMyLocal: (myLocalId) => ({
    type: types.SET_USER_MY_LOCAL_REQUEST,
    payload: { myLocalId },
  }),
  logout: () => ({
    type: types.LOGOUT_REQUEST,
  }),
};
