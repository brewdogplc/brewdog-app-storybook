export const types = {
  GET_BARS_REQUEST: 'GET_BARS_REQUEST',
  GET_BARS_SUCCESS: 'GET_BARS_SUCCESS',
  GET_BARS_FAILURE: 'GET_BARS_FAILURE',
};

export const initialState = {
  data: null,
  loading: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_BARS_REQUEST:
      return { ...state, loading: true, error: null };
    case types.GET_BARS_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case types.GET_BARS_FAILURE:
      return { ...state, loading: false, error: action.error };
    default:
      return state;
  }
};

export const actions = {
  getBars: (options) => ({
    type: types.GET_BARS_REQUEST,
    payload: options,
  }),
};
