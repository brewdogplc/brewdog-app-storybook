export const types = {
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FAILURE: 'LOGIN_FAILURE',
  LOGOUT: 'LOGOUT',
  REFRESH_TOKEN_REQUEST: 'REFRESH_TOKEN_REQUEST',
  REFRESH_TOKEN_SUCCESS: 'REFRESH_TOKEN_SUCCESS',
  REFRESH_TOKEN_FAILURE: 'REFRESH_TOKEN_FAILURE',
};

export const initialState = {
  userId: null,
  loading: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return { ...state, loading: true, error: null };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        userId: action.userId,
      };
    case types.LOGIN_FAILURE:
      return { ...state, loading: false, error: action.error };
    case types.REFRESH_TOKEN_REQUEST:
      return { ...state, loading: true, error: null };
    case types.REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        loading: false,
        userId: action.userId,
      };
    case types.REFRESH_TOKEN_FAILURE:
      return { ...state, loading: false, error: action.error };
    case types.LOGOUT:
      return { ...initialState };
    default:
      return state;
  }
};

export const actions = {
  login: (username, password) => ({
    type: types.LOGIN_REQUEST,
    payload: { username, password },
  }),
  refreshToken: (token) => ({
    type: types.REFRESH_TOKEN_REQUEST,
    payload: { token },
  }),
};
