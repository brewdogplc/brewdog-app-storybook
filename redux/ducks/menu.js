export const types = {
  GET_MENU_REQUEST: 'GET_MENU_REQUEST',
  GET_MENU_SUCCESS: 'GET_MENU_SUCCESS',
  GET_MENU_FAILURE: 'GET_MENU_FAILURE',
};

export const initialState = {
  data: null,
  loading: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_MENU_REQUEST:
      return { ...state, loading: true, error: null };
    case types.GET_MENU_SUCCESS:
      return { ...state, loading: false, data: action.data };
    case types.GET_MENU_FAILURE:
      return { ...state, data: null, loading: false, error: action.error };
    default:
      return state;
  }
};

export const actions = {
  getMenu: (id, sortBy) => ({
    type: types.GET_MENU_REQUEST,
    payload: { id, sortBy },
  }),
};
