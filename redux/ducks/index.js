import { combineReducers } from 'redux';
import bars from './bars';
import menu from './menu';
import user from './user';
import auth from './auth';

export default combineReducers({
  bars,
  menu,
  user,
  auth,
});
