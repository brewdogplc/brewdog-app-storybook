import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet, Text, SectionList, Button } from 'react-native';
import { useNetInfo } from '@react-native-community/netinfo';

import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../redux/ducks/menu';

import BarListHeader from './BarListHeader';
import MenuListItem from './MenuListItem';
import MenuListItemSkeleton from './MenuListItemSkeleton';

import colors from '../constants/colors';
import fonts from '../constants/fonts';

const BarMenuList = ({ bar }) => {
  const netInfo = 'unknown';//useNetInfo();
  const [sort, setSort] = useState(null);
  const previousSort = usePrevious(sort);
  const previousMenuId = usePrevious(bar.untappdLiveTapMenuId);

  //redux
  const dispatch = useDispatch();
  const menuData = useSelector((state) => state.menu.data);
  const menuDataLoading = useSelector((state) => state.menu.loading);
  const menuDataError = useSelector((state) => state.menu.error);

  //loads menu data
  function loadBarMenu() {
    if (!menuDataLoading && (netInfo.isConnected || netInfo.type === 'unknown'))
      dispatch(actions.getMenu(bar.untappdLiveTapMenuId, sort));
  }

  //set the sort in state
  function handleMenuSort(value) {
    setSort(value);
  }

  //function to store previous state of a state prop
  function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  }

  function handleTryAgainPress() {
    loadBarMenu();
  }

  //any changes to bar or sort
  useEffect(() => {
    if (!previousMenuId) loadBarMenu(); //load menu when first opened
    if (previousSort != sort) loadBarMenu();
  }, [bar, sort]);

  //TODO
  //no internet
  if (!netInfo.isConnected)
    return (
      <>
        <BarListHeader bar={bar} />
        <Text style={{ textAlign: 'center' }}>No internet connection</Text>
        <Button title='Try Again' onPress={handleTryAgainPress} />
      </>
    );

  //TODO
  //error
  if (menuDataError)
    return (
      <>
        <BarListHeader bar={bar} />
        <Text style={{ textAlign: 'center' }}>
          Uh oh... Looks like somethings broken at the moment
        </Text>
        <Button title='Try Again' onPress={handleTryAgainPress} />
      </>
    );

  //loading
  if (menuDataLoading)
    return (
      <>
        <BarListHeader bar={bar} />
        <MenuListItemSkeleton />
        <MenuListItemSkeleton />
        <MenuListItemSkeleton />
      </>
    );

  if (menuData)
    return (
      <SectionList
        bounces={false}
        sections={menuData.sections}
        keyExtractor={(item, index) => item + index}
        ListHeaderComponent={() => (
          <BarListHeader
            bar={bar}
            menu={menuData}
            onSortBy={handleMenuSort}
            sort={sort}
          />
        )}
        renderItem={({ item }) => <MenuListItem {...item} />}
        renderSectionHeader={({ section: { name } }) => (
          <Text style={styles.header}>{name.toUpperCase()}</Text>
        )}
      />
    );

  //empty
  //TODO
  return <Text>EMPTY</Text>;
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.BLACK,
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 18,
    lineHeight: 22,
    paddingHorizontal: 24,
    paddingVertical: 8,
    color: colors.WHITE,
  },
});

export default BarMenuList;
