import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import HeaderSettingsButton from '../HeaderSettingsButton';

import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'HeaderSettingsButton',
  state: '',
  color: '#000',
};


storiesOf('HeaderSettingsButton', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <HeaderSettingsButton onPress={null} color={'black'}  />);