import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import BarsListItemSkeleton from '../BarsListItemSkeleton';

import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'BarsListItemSkeleton'
};


storiesOf('BarsListItemSkeleton', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <BarsListItemSkeleton task={task} />);