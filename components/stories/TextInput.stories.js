import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import TextInput from '../TextInput';

import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'TextInput',
  state: '',
  label: 'Test input Label'
  
};


storiesOf('TextInput', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <TextInput label={'label text input'} />);