import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import MenuListItemSkeleton from '../MenuListItemSkeleton';

import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'MenuListItemSkeleton'
};


storiesOf('MenuListItemSkeleton', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <MenuListItemSkeleton task={task} />);