import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import HeaderBackButton from '../HeaderBackButton';
import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'HeaderBackButton',
  state: '',
  color: 'black',
  
};


storiesOf('HeaderBackButton', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <HeaderBackButton onPress={false} color={'black'} />);