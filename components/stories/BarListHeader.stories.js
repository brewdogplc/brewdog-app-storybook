import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import BarListheader from '../BarListheader';

import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'BarListheader',
  bar: {
      name: 'BAR NAME TEST',
      distance: 10
  }
};
export const bar = {
  name: 'BAR NAME TEST',
  distance: 10
};



storiesOf('BarListheader', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <BarListheader bar={bar}/>);