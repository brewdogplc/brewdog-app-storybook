import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import HeaderCloseButton from '../HeaderCloseButton';

import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'HeaderCloseButton',
  state: '',
  color: 'black',
  
};


storiesOf('HeaderCloseButton', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <HeaderCloseButton onPress={true} color={'black'} />);