import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import SettingsListItem from '../SettingsListItem';
import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'SettingsListItem',
  state: '',
  label: 'Test label ',
  onPress: true,
  value: 1
  
};


storiesOf('SettingsListItem', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <SettingsListItem task={task} label={'label goes here'} onPress={true} value={1}/>);