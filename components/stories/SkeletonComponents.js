import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import BarsListItemSkeleton from '../BarsListItemSkeleton';


import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'SkeletonComponents'
};


storiesOf('SkeletonComponents', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('BarsListItemSkeleton', () => <BarsListItemSkeleton task={task} />)