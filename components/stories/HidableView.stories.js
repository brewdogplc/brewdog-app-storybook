import * as React from 'react';
import { View, Text } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import HidableView from '../HidableView';
import Iphone11 from '../StoriesViewContainers/iphone11';
import DATA from './testData/bar_data.json';
export const task = {
  id: '1',
  title: 'HidableView',
  state: '',
  name: 'name',
  distance: '22',
  bar: DATA[0]
};


storiesOf('HidableView', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <HidableView visible={true} height={220} children={<Text>test</Text>}/>);