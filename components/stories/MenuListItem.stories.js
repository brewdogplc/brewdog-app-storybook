import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import MenuListItem from '../MenuListItem';
import menu_data from './testData/menu.json'
import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'MenuListItem',
  state: '',
  name: 'name',
  brewery: 'brewery',
  style: 'strong one',
  abv: '99',
  rating: '5',
  menu: menu_data.menu.sections[0].data[0]
  
};


storiesOf('MenuListItem', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <MenuListItem 
    name={'name'} 
    //menu={menu_data.menu.sections[0].data[0]}
    brewery={'brewery'}
    style={'strong'}
    abv={'99'}
    rating={'5'}
  />);