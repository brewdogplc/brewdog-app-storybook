import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import BarsListItem from '../BarsListItem';
import Iphone11 from '../StoriesViewContainers/iphone11';
import DATA from './testData/bar_data.json';
export const task = {
  id: '1',
  title: 'BarsListItem',
  state: '',
  name: 'name',
  distance: '22',
  bar: DATA[0]
};


storiesOf('BarsListItem', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <BarsListItem name={'name'} bar={DATA[0]} distance={'22'}/>);