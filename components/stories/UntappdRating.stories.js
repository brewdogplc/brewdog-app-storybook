import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import UntappdRating from '../UntappdRating';
export const task = {
  id: '1',
  title: 'UntappdRating',
  state: 'Rating',
  rating: null
};

storiesOf('UntappdRating', module)
  .addDecorator(story => <View>{story()}</View>)
  .add('default', () => <UntappdRating task={task} />);