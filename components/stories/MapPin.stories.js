import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import MapPin from '../MapPin';
export const task = {
  id: '1',
  title: 'TextInput',
  state: '',
  children: null,
  
};


storiesOf('MapPin', module)
  .addDecorator(story => <View>{story()}</View>)
  .add('default', () => <MapPin task={task} />);