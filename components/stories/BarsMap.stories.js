import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import BarsMap from '../BarsMap';
import Iphone11 from '../StoriesViewContainers/iphone11';
import DATA from './testData/bar_data.json';
export const task = {
  id: '1',
  title: 'BarsMap',
  state: '',
  name: 'name',
  distance: '22',
  bar: DATA[0]
};


storiesOf('BarsMap', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <BarsMap visible={true} height={500} />);