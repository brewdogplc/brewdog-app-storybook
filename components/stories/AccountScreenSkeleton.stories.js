import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import AccountScreenSkeleton from '../AccountScreenSkeleton';

import Iphone11 from '../StoriesViewContainers/iphone11';
export const task = {
  id: '1',
  title: 'AccountScreenSkeleton'
};


storiesOf('AccountScreenSkeleton', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <AccountScreenSkeleton task={task} />);