import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import Button from '../Button';

import Icon from '../BrewDogIcon';

import Iphone11 from '../StoriesViewContainers/iphone11';

const styles = StyleSheet.create({
  roundedWhite: {
    flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 46,
      backgroundColor:'white',
      color: 'black',
      borderColor: 'black',
      borderWidth: 2,
      borderRadius: 23
  },
  roundedBlack: {
    flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 46,
      backgroundColor:'black',
      color: 'white',
      borderRadius: 23,
  },
  roundedBlackBordered: {
    flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 46,
      backgroundColor:'black',
      color: 'white',
      borderRadius: 23,
      borderColor: 'white',
      borderWidth: 2,
  },
  defaultStyle: {
     flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 46,
      backgroundColor: 'black'
  },
  defaultStyleWhite: {
     flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 46,
      backgroundColor: 'white'
  },
  bdBlue: {
     flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 46,
      backgroundColor: '#00AFDB'
  },
  disabledButton: {
    flexDirection: 'row',
     justifyContent: 'center',
     alignItems: 'center',
     width: '100%',
     height: 46,
     backgroundColor: '#B3B3B3'
 },
  blackText: {
    color: 'black'
  },
  whiteText: {
    color: 'white'
  }
});

export const defaultStyle = {
  id: '1',
  title: 'BUTTON',
  state: 'button',
  icon: <Icon name='location' size={24} color={'white'} />,
  color: 'white',
  buttonStyle: styles.defaultStyle,
  textStyle: styles.whiteText,
};
export const defaultStyleWhite = {
  id: '1',
  title: 'BUTTON',
  state: 'button',
  icon: <Icon name='location' size={24} color={'white'} />,
  color: 'white',
  buttonStyle: styles.defaultStyleWhite,
  textStyle: styles.blackText,
};
export const roundedWhite = {
  id: '1',
  title: 'roundedWhite',
  state: 'button',
  icon: <Icon name='location' size={24} color={'black'} />,
  color: 'white',
  buttonStyle: styles.roundedWhite,
  textStyle: styles.blackText
};

export const roundedBlack = {
  id: '1',
  title: 'roundedBlack',
  state: 'button',
  icon: <Icon name='location' size={24} color={'white'} />,
  color: 'white',
  buttonStyle: styles.roundedBlack,
  textStyle: styles.whiteText
};
export const roundedBlackBordered = {
  id: '1',
  title: 'roundedBlackBordered',
  state: 'button',
  icon: <Icon name='location' size={24} color={'white'} />,
  color: 'white',
  buttonStyle: styles.roundedBlackBordered,
  textStyle: styles.whiteText
};

export const disabledButton = {
  id: '1',
  title: 'disabledButton',
  state: 'button',
  icon: <Icon name='location' size={24} color={'white'} />,
  color: 'white',
  buttonStyle: styles.disabledButton,
  textStyle: styles.whiteText
};

export const bdBlueButton = {
  id: '1',
  title: 'bdBlueButton',
  state: 'button',
  icon: null,
  color: 'white',
  buttonStyle: styles.bdBlue,
  textStyle: styles.blackText
};

export const actions = {
  onPinTask: action('onPinTask'),
  onArchiveTask: action('onArchiveTask'),
};
storiesOf('Button', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('defaultStyle', () => <Button buttonStyle={defaultStyle} textStyle={null} title={'button'}
   icon={null} color={'white'} testicon={null}/>);
  /*.add('RoundedWhite', () => <Button task={roundedWhite} {...actions} />)
  .add('roundedBlack', () => <Button task={roundedBlack} {...actions} />)
  .add('disabledButton', () => <Button task={disabledButton} {...actions} />)
  .add('defaultStyleWhite', () => <Button task={defaultStyleWhite} {...actions} />)
  .add('bdBlue', () => <Button task={bdBlueButton} {...actions} />)
  .add('roundedBlackBordered', () => <Button task={roundedBlackBordered} {...actions} />);*/