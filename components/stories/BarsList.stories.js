import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import BarList from '../BarsList';

import Iphone11 from '../StoriesViewContainers/iphone11';

export const bar = {
  name: 'BAR NAME TEST',
  distance: 10
};



storiesOf('BarList', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <BarList bar={bar}/>);