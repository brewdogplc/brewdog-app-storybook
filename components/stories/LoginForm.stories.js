import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import LoginForm from '../LoginForm';

import Iphone11 from '../StoriesViewContainers/iphone11';

export const bar = {
  name: 'BarMenuList',
  distance: 10
};



storiesOf('LoginForm', module)
  .addDecorator(story => <Iphone11 storyComponent={story()}></Iphone11>)
  .add('default', () => <LoginForm/>);