import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import UntappdRating from './UntappdRating';
import fonts from '../constants/fonts';
import colors from '../constants/colors';

import PropTypes from 'prop-types';

export default class MenuListItem extends React.PureComponent {
  render() {
    //const { menu } = this.props.task;
    const { name, brewery, style, abv, rating } = this.props;

    return (
      <View style={styles.container}>

        <Text style={styles.name}>{name.toUpperCase()}</Text>
        <Text style={styles.brewery}>{brewery}</Text>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.beerStyle}>{style}</Text>
          <Text
            style={[styles.abv, style ? { paddingHorizontal: 8 } : null]}
          >{`ABV ${abv}%`}</Text>
        </View>
        <UntappdRating rating={rating} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.SILVER
  },
  name: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 24,
    lineHeight: 24,
    color: colors.BLACK
  },
  brewery: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.BLACK
  },
  beerStyle: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.BLACK
  },
  abv: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.LIMEADE
  }
});


MenuListItem.propTypes = {
  name:  PropTypes.string,
  brewery:  PropTypes.string,
  style:  PropTypes.string,
  abv:  PropTypes.string,
  rating:  PropTypes.string
};
