import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, View, Text } from 'react-native';

import Icon from './BrewDogIcon';
//import strings from '../constants/strings';
import fonts from '../constants/fonts';
import PropTypes from 'prop-types';

export default function HeaderBackButton({ color, onPress }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <Icon name='arrow-left' color={color} size={15} />
        <Text
          style={{
            color,
            ...styles.text
          }}
        >
          {'Back'}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 7
  },
  text: {
    paddingLeft: 10,
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 14
  }
});


HeaderBackButton.propTypes = {
  onPress: PropTypes.func,
  color:  PropTypes.string
};
