import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';

import colors from '../constants/colors';
import fonts from '../constants/fonts';

import Icon from './BrewDogIcon';
import PropTypes from 'prop-types';

export default class SettingsListItem extends React.PureComponent {
  render() {
    const { onPress, label, value } = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Text style={styles.label}>{label}</Text>
        {onPress ? (
          <Icon name='arrow-right' color={colors.BLACK} size={13} />
        ) : (
          <Text style={styles.label}>{value}</Text>
        )}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 24,
    borderBottomWidth: 1,
    borderBottomColor: colors.SILVER
  },
  label: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22
  }
});

SettingsListItem.propTypes = {
  label: PropTypes.string,
  value: PropTypes.number,
  onPress: PropTypes.number
};
