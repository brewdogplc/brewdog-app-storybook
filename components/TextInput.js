import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import PropTypes from 'prop-types';

import fonts from '../constants/fonts';
import colors from '../constants/colors';


export default class BrewDogTextInput extends Component {
  textInputRef = React.createRef();

  focus = () => {
    if (this.textInputRef.current) {
      this.textInputRef.current.focus();
    }
  };

  render() {
    const { label } = this.props;
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
    <Text style={styles.labelText}>{label}</Text>
          {/*inlineLabelComponent*/}
        </View>
        <TextInput
          ref={this.textInputRef}
          placeholderTextColor={colors.GREY}
          autoCapitalize='none'
          autoCorrect={false}
          
          style={styles.inputText}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { width: '100%', marginVertical: 10 },
  labelText: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 14,
    lineHeight: 22,
    color: colors.BLACK
  },
  inputText: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 14,
    color: colors.BLACK,
    backgroundColor: colors.WHITE,
    height: 50,
    paddingHorizontal: 16,
    paddingVertical: 12,
    marginTop: 8,
    borderWidth: 1,
    borderColor: colors.SILVER
  }
});

BrewDogTextInput.propTypes = {
  label: PropTypes.string.isRequired
};
