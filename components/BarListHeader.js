import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
//import RNPickerSelect from 'react-native-picker-select';

import colors from '../constants/colors';
import fonts from '../constants/fonts';
//import Moment from 'moment'

//import getNumberOfTaps from '../utils/GetNumberOfTaps'

import Button from './Button';
import Icon from './BrewDogIcon';
import PropTypes from 'prop-types';


import BREWDOG_SHIELD_WHITE from '../assets/images/brewdog_shield_white.png';



const filterOptions = [
  {
    label: 'ABV descending',
    value: 'ABV descending'
  },
  {
    label: 'ABV ascending',
    value: 'ABV ascending'
  },
  {
    label: 'Rating descending',
    value: 'Rating descending'
  },
  {
    label: 'Rating ascending',
    value: 'Rating ascending'
  }
];

export default class BarListHeader extends React.PureComponent {
  render() {
    const { bar, menu, onSortBy, sort } = this.props;
    

    return (
      <>
        <View style={styles.barDetailsContainer}>
          <View style={styles.barDetailsRow}>
            <View style={styles.barDetailsCard}>
            {/*<BarOpeningHours openingHours={bar.openingHours} style={styles.openingHours} />*/}
            <View style={styles.openingHours}>
            <Text>Open until 12am</Text>
          </View>
              <Text style={styles.barName}>{bar.name.toUpperCase()}</Text>
              <View style={styles.distanceContainer}>
                <Icon name='location' size={14} color={colors.BREWDOG_BLUE} />
                <Text style={styles.distance}>{bar.distance+"mi"}</Text>
              </View>
            </View>
            <Image source={BREWDOG_SHIELD_WHITE} style={styles.brewDogShield} />
          </View>
          <Button
            task={{
                id: '1',
                title: 'BUTTON',
                state: 'button',
                icon: <Icon name='location' size={24} color={colors.BLACK} />,
                color: 'white',
                buttonStyle: styles.headerButton,
                textStyle: styles.headerButtonText
            }}
            title='Get directions'
            icon={() => <Icon name='location' size={24} color={colors.BLACK} />}
            onPress={() => {}}
            buttonStyle={styles.headerButton}
            textStyle={styles.headerButtonText}
          />
          <Button
          task={{
                id: '1',
                title: 'BUTTON',
                state: 'button',
                icon: <Icon name='clock' size={24} color={colors.BLACK} />,
                color: 'white',
                buttonStyle: styles.headerButton,
                textStyle: styles.headerButtonText
            }}
            title='Book a table'
            icon={() => <Icon name='clock' size={24} color={colors.BLACK} />}
            onPress={() => {}}
            buttonStyle={styles.headerButton}
            textStyle={styles.headerButtonText}
          />
          <Button
            task={{
                id: '1',
                title: 'BUTTON',
                state: 'button',
                icon: <Icon name='book' size={24} color={colors.BLACK} />,
                color: 'white',
                buttonStyle: styles.headerButton,
                textStyle: styles.headerButtonText
            }}
            title='View menu'
            icon={() => <Icon name='book' size={24} color={colors.BLACK} />}
            onPress={() => {}}
            buttonStyle={styles.headerButton}
            textStyle={styles.headerButtonText}
          />
        </View>
        <View style={styles.tapListHeaderContainer}>
          <Text style={styles.tapListTitle}>{`Tap List`.toUpperCase()}</Text>
          {menu ? (
            <View>
              {/*<Text style={styles.lastUpdated}>{`Last updated ${Moment.duration(Moment().diff(menu.updated_at))
                  .humanize()} ago`}</Text>*/}
                  <Text style={styles.lastUpdated}>{`Last updated 10 hours ago`}</Text>
         {/*<Text style={styles.numberOfBeers}>{` ${getNumberOfTaps(menu.menu)} beers available`}</Text>*/}
         <Text style={styles.numberOfBeers}>{` 99 beers available`}</Text>
           </View>
          ) : (
            <View>
             <Text style={styles.lastUpdated}>{`Last updated ... ago`}</Text>
             <Text style={styles.numberOfBeers}>{` ... beers available`}</Text>
            </View> 
          )}
          {/*<RNPickerSelect
            items={filterOptions}
            onValueChange={value => {}} //TODO
            useNativeAndroidPickerStyle={false}
            placeholder={{
              label: 'Sort by',
              value: null,
              color: colors.BLACK
            }}
            textInputProps={{ style: styles.input }}
            style={{
              inputIOS: styles.input,
              inputAndroid: styles.input,
              iconContainer: { top: 13, right: 15 },
              placeholder: {
                color: colors.BLACK
              }
            }}
            Icon={() => (
              <Icon name='arrow-down' size={15} color={colors.BLACK} />
            )}
            />*/}
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  barDetailsContainer: {
    backgroundColor: colors.BLACK,
    flex: 1,
    padding: 24
  },
  barDetailsRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  barDetailsCard: { flexShrink: 1 },
  openingHours: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    color: colors.WHITE,
    paddingBottom: 10
  },
  barName: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 48,
    lineHeight: 48,
    color: colors.WHITE
  },
  distanceContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 8
  },
  distance: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.WHITE,
    paddingHorizontal: 4
  },
  brewDogShield: {
    height: 117,
    width: 83,
    resizeMode: 'contain',
    opacity: 0.2
  },
  headerButton: { backgroundColor: colors.WHITE, marginTop: 8 },
  headerButtonText: { color: colors.BLACK },
  tapListHeaderContainer: { width: '100%', padding: 24 },
  tapListTitle: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 48,
    lineHeight: 48,
    color: colors.BLACK
  },
  lastUpdated: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.BLACK
  },
  numberOfBeers: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.BLACK,
    paddingBottom: 16
  },
  input: {
    backgroundColor: colors.WHITE,
    height: 45,
    paddingHorizontal: 15,
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    borderWidth: 1,
    borderColor: colors.NOBEL,
    color: colors.BLACK
  }
});

BarListHeader.propTypes = {
  menu: PropTypes.object,
  bar:  PropTypes.object,
  onSortBy: PropTypes.func,
  sort: PropTypes.string
};
