import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './BrewDogIcon/fontelloConfig.json';
export default createIconSetFromFontello(fontelloConfig);
