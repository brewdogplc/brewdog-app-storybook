// components/Task.js
import * as React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';

import colors from '../constants/colors';
import fonts from '../constants/fonts';
import Icon from './BrewDogIcon';

export default function Button({ buttonStyle, textStyle, title, icon, color, testicon} ) {
  
  return (
    <TouchableOpacity style={[styles.container, buttonStyle]}>
      {/*{icon ? icon() : null}*/}
      {icon}
      <Text style={[styles.text, textStyle, icon ? { paddingLeft: 8 } : null]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}


const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: 46,
      backgroundColor: colors.BLACK
    },
    text: {
      fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
      fontSize: 14,
      color: colors.WHITE
    }
  });
  