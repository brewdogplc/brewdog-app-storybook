import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Shine,
} from 'rn-placeholder';

import colors from '../constants/colors';

export default class BarsListItem extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Placeholder
          Animation={Shine}
          Right={(props) => (
            <PlaceholderMedia
              style={[{ height: 20 }, { width: 40 }, { marginTop: 20 }]}
            />
          )}
        >
          <PlaceholderLine width={40} style={{ marginTop: 15 }} />
          <PlaceholderLine width={80} height={20} />
        </Placeholder>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    height: 95,
    borderTopWidth: 1,
    borderTopColor: colors.SILVER,
  },
});
