import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

import colors from '../constants/colors';
import fonts from '../constants/fonts';

import Icon from './BrewDogIcon';

import PropTypes from 'prop-types';



export default class BarsListItem extends React.PureComponent {
  render() {
    const { name, distance, bar } = this.props;
    const unit = 'mi'
    return (
      <TouchableOpacity style={styles.container} >
        <View style={styles.nameContainer}>
        {/*<BarOpeningHours openingHours={openingHours} style={styles.openingHours} />*/}
          <View style={styles.openingHours}>
            <Text>Open until 12am</Text>
          </View>
          <Text style={styles.name}>{name.toUpperCase()}</Text>
        </View>
        <View style={styles.distanceContainer}>
          <Icon name='location' size={15} color={colors.BREWDOG_BLUE} />
          <Text style={styles.distance}>{`${distance} ${unit}`}</Text>
        </View>
        <View>
          <Icon name='arrow-right' size={15} />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 24,
    height: 95,
    borderTopWidth: 1,
    borderTopColor: colors.SILVER
  },
  nameContainer: { flexGrow: 1 },
  openingHours: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16
  },
  name: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 24
  },
  distanceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  distance: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    paddingHorizontal: 4
  }
});

BarsListItem.propTypes = {
  name: PropTypes.string,
  bar:  PropTypes.object,
  distance: PropTypes.string
};
