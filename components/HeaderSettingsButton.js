import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, Text, View } from 'react-native';

import Icon from './BrewDogIcon';
import fonts from '../constants/fonts';

import PropTypes from 'prop-types';

export default class HeaderSettingsButton extends React.PureComponent {
  render() {
    const { onPress, color } = this.props;

    return (
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.container}>
          <Text style={[styles.settings, { color }]}>Settings</Text>
          <Icon name='gear' color={color} size={25} style={styles.icon} />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  settings: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 14,
    lineHeight: 22
  },
  icon: { paddingRight: 15, paddingLeft: 8 }
});

HeaderSettingsButton.propTypes = {
  onPress: PropTypes.string,
  color:  PropTypes.string
};
