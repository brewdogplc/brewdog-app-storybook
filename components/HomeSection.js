import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import fonts from '../constants/fonts';

export default function HomeSection({ task: {title} }) {

  return (
    <>
      <Text style={styles.sectionTitle}>{title}</Text>
      <View style={styles.sectionContainer}></View>
    </>
  );
}

const styles = StyleSheet.create({
  sectionTitle: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 18,
    lineHeight: 22,
    alignSelf: 'flex-start',
    paddingTop: 32,
    paddingBottom: 16
  },
  sectionContainer: { width: '100%' }
});
