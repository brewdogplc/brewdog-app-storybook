import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import MapView from 'react-native-maps';
import { useNavigation } from '@react-navigation/native';
import { useNetInfo } from '@react-native-community/netinfo';

import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../redux/ducks/bars';

import HidableView from '../components/HidableView';
import MapPin from '../components/MapPin';
import Icon from '../components/BrewDogIcon';
import Button from '../components/Button';

import fonts from '../constants/fonts';
import colors from '../constants/colors';

//TODO get this for real
/*const currentLocation = {
  latitude: 57.288559,
  longitude: -2.37873,
};*/
const currentLocation = null;

const BarsMap = () => {
  const navigation = useNavigation();
  const netInfo = useNetInfo();
  const [showBarCard, setShowBarCard] = useState(false);
  const [selectedBar, setSelectedBar] = useState(null);

  function handleOpenBar(data) {
    navigation.navigate('bar', { bar: { ...data } });
  }

  function handleShowBarCard(bar) {
    if (!selectedBar) {
      setShowBarCard(true);
      setSelectedBar(bar);
    } else {
      setSelectedBar(bar);
    }
  }

  function handleHideBarCard(event) {
    if (event.nativeEvent.action !== 'marker-press') setShowBarCard(false);
  }

  function onBarCardHidden() {
    setSelectedBar(null);
  }

  //redux
  const dispatch = useDispatch();
  const barsData = useSelector((state) => state.bars.data);
  const barsDataLoading = useSelector((state) => state.bars.loading);
  const barsDataError = useSelector((state) => state.bars.error);

  //if there is no bardata in the redux store, no error and it isnt loading get bars data
  if (!barsData && !barsDataError && !barsDataLoading && netInfo.isConnected)
    dispatch(
      actions.getBars({
        location: currentLocation,
      })
    );

  return (
    <>
      <MapView
        style={{ ...StyleSheet.absoluteFillObject }}
        onPress={handleHideBarCard}
      >
        {barsData
          ? barsData.map((bar) => {
              return (
                <MapPin
                  key={bar.name}
                  name={bar.name}
                  onPress={() => handleShowBarCard(bar)}
                  coordinate={{
                    latitude: Number(bar.location.lat),
                    longitude: Number(bar.location.lon),
                  }}
                />
              );
            })
          : null}
      </MapView>
      <HidableView
        visible={showBarCard}
        height={220}
        onHidden={() => onBarCardHidden()}
        style={styles.barCardContainer}
      >
        {selectedBar ? (
          <>
            <TouchableOpacity
              onPress={() => handleOpenBar(selectedBar)}
              style={styles.barCard}
            >
              <View style={styles.barDetailsContainer}>
                <Text style={styles.barName}>
                  {selectedBar.name.toUpperCase()}
                </Text>
                {selectedBar.distance ? (
                  <View style={styles.distanceContainer}>
                    <Icon
                      name='location'
                      color={colors.BREWDOG_BLUE}
                      size={16}
                    />
                    <Text style={styles.distance}>{`${
                      selectedBar.distance
                    } mi`}</Text>
                  </View>
                ) : (
                  <View style={{ height: 22 }} />
                )}
                <Text style={styles.status}>{selectedBar.openStatus}</Text>
              </View>
              <Icon name='arrow-right' color={colors.WHITE} />
            </TouchableOpacity>
            <Button
              title={'Get Directions'}
              buttonStyle={styles.directionsButton}
              onPress={() => {}}
            />
          </>
        ) : null}
      </HidableView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  listHeaderContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
  },
  listHeaderTitle: { fontFamily: fonts.NEW_GROTESK_SQUARE, fontSize: 32 },
  listHeaderSubTitle: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
  },
  barCardContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingTop: 16,
  },
  barCard: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 8,
  },
  barDetailsContainer: { flexGrow: 1 },
  openingHours: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.WHITE,
  },
  barName: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 24,
    color: colors.WHITE,
    paddingBottom: 8,
  },
  distanceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  distance: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.WHITE,
    paddingHorizontal: 4,
  },
  directionsButton: { borderWidth: 1, borderColor: colors.WHITE },
  status: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.WHITE,
  },
});

export default BarsMap;
