import React from 'react';
import {
  StyleSheet,
  Image,
  StatusBar,
  Text,
  KeyboardAvoidingView,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../redux/ducks/auth';

import fonts from '../constants/fonts';
import colors from '../constants/colors';

import TextInput from '../components/TextInput';
import Button from '../components/Button';

import BREWDOG_SHIELD from '../assets/images/brewdog_shield_large.png';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function LoginForm() {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  //redux
  const dispatch = useDispatch();
  const authLoading = useSelector((state) => state.auth.loading);
  const authError = useSelector((state) => state.auth.error);

  function handleLoginButton() {
    dispatch(actions.login(username, password));
  }

  useFocusEffect(
    React.useCallback(() => {
      StatusBar.setBarStyle('dark-content');
    }, [])
  );

  //TODO handle error
  //TODO handle loading
  return (
    <KeyboardAvoidingView behavior='position' style={styles.formContainer}>
      <Image source={BREWDOG_SHIELD} style={styles.brewDogShield} />
      <Text style={styles.yourAccount}>{`YOUR ACCOUNT`.toUpperCase()}</Text>
      <Text style={styles.description}>
        Log in with your existing BrewDog.com account or create a new account.
      </Text>
      <TextInput
        label='Email Address'
        placeholder='Enter your email address'
        textContentType='username'
        keyboardType='email-address'
        returnKeyType='next'
        onChangeText={setUsername}
        onSubmitEditing={() => {}}
      />
      <TextInput
        ref={this.passwordInputRef}
        label='Password'
        placeholder='Enter your password'
        textContentType='password'
        secureTextEntry
        returnKeyType='done'
        onChangeText={setPassword}
        inlineLabelComponent={
          <TouchableOpacity onPress={() => {}}>
            <Text style={styles.forgotPassword}>I forgot my password</Text>
          </TouchableOpacity>
        }
      />
      <Button
        title='Login'
        onPress={handleLoginButton}
        buttonStyle={styles.loginButton}
      />
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 24,
  },
  brewDogShield: { height: 96, resizeMode: 'contain', alignSelf: 'center' },
  yourAccount: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 48,
    lineHeight: 48,
    textAlign: 'center',
    color: colors.BLACK,
    paddingVertical: 24,
  },
  description: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 14,
    lineHeight: 22,
    textAlign: 'center',
    color: colors.BLACK,
    paddingBottom: 32,
  },
  forgotPassword: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 14,
    lineHeight: 22,
    textDecorationLine: 'underline',
  },
  loginButton: { marginTop: 24 },
});
