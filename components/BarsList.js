import React, { useState, useEffect } from 'react';
import { StyleSheet, View, FlatList, Text, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useNetInfo } from "@react-native-community/netinfo";

import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../redux/ducks/bars';

import BarsListItem from './BarsListItem';
import BarsListItemSkeleton from './BarsListItemSkeleton';

import fonts from '../constants/fonts';

//TODO get this for real
/*const currentLocation = {
  latitude: 57.288559,
  longitude: -2.37873,
};*/
const currentLocation = null;

const BarsList = () => {
  const navigation = useNavigation();
  const netInfo = 'unknown';//useNetInfo();
  const [refreshing, setRefreshing] = useState(false);

  //set refreshing to true and get bars from punk api
  function handleOnRefresh() {
    //only refresh if there is internet
    if (netInfo.isConnected) {
      setRefreshing(true);
      dispatch(
        actions.getBars({
          location: currentLocation,
        })
      );
    }
  }

  //redux
  const dispatch = useDispatch();
  const barsData = useSelector((state) => state.bars.data);
  const barsDataLoading = useSelector((state) => state.bars.loading);
  const barsDataError = useSelector((state) => state.bars.error);

  //if there is no bardata in the redux store, no error, online and it isnt loading get bars data
  if (!barsData && !barsDataError && !barsDataLoading && netInfo.isConnected)
    dispatch(
      actions.getBars({
        location: currentLocation,
      })
    );

  //set refreshing to false after data is loaded if refreshing is true
  function handleBarDataLoaded() {
    if (refreshing) setRefreshing(false);
  }

  function handleOpenBar(data) {
    navigation.navigate('bar', { bar: data });
  }

  //try to load data again after no internet connection or error
  function handleTryAgainPress() {
    if (netInfo.isConnected) {
      dispatch(
        actions.getBars({
          location: currentLocation,
        })
      );
    }
  }

  //track any changes to refreshing and barsDataLoading
  useEffect(() => {
    if (refreshing && !barsDataLoading) handleBarDataLoaded();
  }, [refreshing, barsDataLoading]);

  //TODO offline and no data
  if (!netInfo.isConnected && !barsData)
    return (
      <View>
        <View style={styles.listHeaderContainer}>
          <Text style={styles.listHeaderTitle}>
            {`Explore our Bars`.toUpperCase()}
          </Text>
          {currentLocation ? (
            <Text style={styles.listHeaderSubTitle}>
              By proximity to your location
            </Text>
          ) : null}
        </View>
        <Text style={{ textAlign: 'center' }}>No internet connection</Text>
        <Button title='Try Again' onPress={handleTryAgainPress} />
      </View>
    );

  //TODO error
  if (barsDataError)
    return (
      <View>
        <View style={styles.listHeaderContainer}>
          <Text style={styles.listHeaderTitle}>
            {`Explore our Bars`.toUpperCase()}
          </Text>
          {currentLocation ? (
            <Text style={styles.listHeaderSubTitle}>
              By proximity to your location
            </Text>
          ) : null}
        </View>
        <Text style={{ textAlign: 'center' }}>
          Uh oh... Looks like somethings broken at the moment
        </Text>
        <Button title='Try Again' onPress={handleTryAgainPress} />
      </View>
    );

  //loading with no data
  if (barsDataLoading && !barsData) {
    return (
      <View>
        <View style={styles.listHeaderContainer}>
          <Text style={styles.listHeaderTitle}>
            {`Explore our Bars`.toUpperCase()}
          </Text>
          {currentLocation ? (
            <Text style={styles.listHeaderSubTitle}>
              By proximity to your location
            </Text>
          ) : null}
        </View>
        <BarsListItemSkeleton />
        <BarsListItemSkeleton />
        <BarsListItemSkeleton />
        <BarsListItemSkeleton />
        <BarsListItemSkeleton />
        <BarsListItemSkeleton />
        <BarsListItemSkeleton />
      </View>
    );
  }

  //success!
  return (
    <FlatList
      data={barsData}
      refreshing={refreshing}
      onRefresh={handleOnRefresh}
      ListHeaderComponent={() => (
        <View style={styles.listHeaderContainer}>
          <Text style={styles.listHeaderTitle}>
            {`Explore our Bars`.toUpperCase()}
          </Text>
          {currentLocation ? (
            <Text style={styles.listHeaderSubTitle}>
              By proximity to your location
            </Text>
          ) : null}
        </View>
      )}
      renderItem={({ item }) => (
        <BarsListItem {...item} onPress={() => handleOpenBar(item)} />
      )}
      keyExtractor={(item) => item.name}
    />
  );
};

const styles = StyleSheet.create({
  listHeaderContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
  },
  listHeaderTitle: { fontFamily: fonts.NEW_GROTESK_SQUARE, fontSize: 32 },
  listHeaderSubTitle: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
  },
});

export default BarsList;
