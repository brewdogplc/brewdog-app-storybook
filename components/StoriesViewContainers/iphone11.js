import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Icon from '../BrewDogIcon';
import fonts from '../../constants/fonts';

export default function iphone11({ storyComponent }) {
  return (
    <View style={styles.iphone11}>
      {storyComponent}
      {/*<View style={styles.mockNav}>
        <View style={styles.mockNavLink}>
          <Icon name='home' size={32} color={'#7d7d7d'} />
          <Text style={styles.mockNavText}>Home</Text>
        </View>
        <View style={styles.mockNavLink}>
          <Icon name='location' size={32} color={'#7d7d7d'} />
        </View>
        <View style={styles.mockNavLink}>
          <Icon name='pint' size={30} color={'#7d7d7d'} />
        </View>
        <View style={styles.mockNavLink}>
          <Icon name='book' size={30} color={'#7d7d7d'} />
        </View>
        <View style={styles.mockNavLink}>
          <Icon name='user' size={30} color={'#7d7d7d'} />
        </View>
  </View>*/}
    </View>
  );
}

const styles = StyleSheet.create({
 iphone11: {
     width: 375,
     height: 812,
     backgroundColor: 'white'
 },
 mockNav: {
  width: '100%',
  height: 90,
  borderTopColor: '#cccccc',
  borderTopWidth: 2,
  bottom: 0,
  flexDirection: "row"
},
mockNavLink: {
  alignContent: "center",
  width: 74,
  justifyContent: "center",
  paddingLeft: 22
},
mockNavText: {
  fontSize: 14,
  color: '#7D7D7D',
  fontWeight: "500",
  fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
}
});
