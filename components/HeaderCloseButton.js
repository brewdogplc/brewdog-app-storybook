import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';

import Icon from './BrewDogIcon';

import PropTypes from 'prop-types';

export default function HeaderCloseButton({ color, onPress} ) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Icon name='cross' color={color} size={15} style={{ padding: 15 }} />
    </TouchableWithoutFeedback>
  );
}

HeaderCloseButton.propTypes = {
  onPress: PropTypes.string,
  color:  PropTypes.string
};
