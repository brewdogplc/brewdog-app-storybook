import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import colors from '../constants/colors';
import fonts from '../constants/fonts';

import PropTypes from 'prop-types'

function Circle({ index, numberOfCircles, percentage }) {
  return (
    <View
      style={[
        styles.circle,
        index === 0 ? { marginLeft: 0 } : null, //first circle
        index === 4 ? { marginRight: 8 } : null, //last circle
        {
          backgroundColor:
            index <= numberOfCircles ? colors.UNTAPPD_YELLOW : colors.NOBEL
        },
        index === numberOfCircles
          ? {
              borderRightColor: colors.NOBEL,
              borderRightWidth: (18 / 100) * (100 - percentage)
            }
          : null
      ]}
    />
  );
}

export default class UntappdRating extends React.PureComponent {
  render() {
    let rating
    if(this.props.rating){
      rating = this.props.rating;
    }else{ 
      rating = 3.503837;
    }
    

    const percentage = (rating % 1) * 100; //decimal part of rating * 100
    const numberOfCircles = Math.floor(rating);

    return (
      <View style={styles.container}>
        <Circle
          index={0}
          numberOfCircles={numberOfCircles}
          percentage={percentage}
        />
        <Circle
          index={1}
          numberOfCircles={numberOfCircles}
          percentage={percentage}
        />
        <Circle
          index={2}
          numberOfCircles={numberOfCircles}
          percentage={percentage}
        />
        <Circle
          index={3}
          numberOfCircles={numberOfCircles}
          percentage={percentage}
        />
        <Circle
          index={4}
          numberOfCircles={numberOfCircles}
          percentage={percentage}
        />
        <Text style={styles.text}>{`(${Number(rating).toFixed(
          2
        )}) Untappd`}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flexDirection: 'row', alignItems: 'center' },
  text: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    color: colors.GREY
  },
  circle: { height: 18, width: 18, borderRadius: 10, marginHorizontal: 2 }
});

Circle.propTypes = {
  rating: PropTypes.number
}