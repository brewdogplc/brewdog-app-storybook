import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
//import { Marker } from 'react-native-maps';

import colors from '../constants/colors';

import Icon from './BrewDogIcon';

export default class MapPin extends React.PureComponent {
  render() {
    //const { coordinate, onPress, selected } = this.props;
    const selected = null;
    return (
      /*<Marker
        tracksViewChanges={false}
        coordinate={coordinate}
        onPress={onPress}
      >*/
      <View>
        <Icon
          name='map-pin'
          size={!selected ? 24 : 32}
          color={!selected ? colors.BLACK : colors.BREWDOG_BLUE}
        />
        </View>
        /*
      </Marker>*/
    );
  }
}
