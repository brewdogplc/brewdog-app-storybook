import React, { Component } from 'react';
import { Text, StyleSheet, Animated, Easing } from 'react-native';

import colors from '../constants/colors';

//TODO turn in functional component
export default class HidableView extends Component {
  constructor(props) {
    super(props);

    const { height } = this.props;
    this.animatedHeight = new Animated.Value(height);
    this.animatedOpacity = new Animated.Value(0);
  }

  componentDidUpdate(prevProps) {
   // if (prevProps.visible !== this.props.visible) {
      this.animateToggle();
    //}
  }

  animateToggle() {
    const { visible, height, onHidden } = this.props;

    //open
    if (visible) {
      Animated.stagger(250, [
        Animated.timing(this.animatedHeight, {
          toValue: 0,
          duration: 500,
          easing: Easing.out(Easing.quad),
          useNativeDriver: true,
        }),
        Animated.timing(this.animatedOpacity, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true,
        }),
      ]).start();
    }

    //close
    if (!visible) {
      Animated.stagger(100, [
        Animated.timing(this.animatedOpacity, {
          toValue: 0.1,
          duration: 500,
          useNativeDriver: true,
        }),
        Animated.timing(this.animatedHeight, {
          toValue: height,
          duration: 500,
          useNativeDriver: true,
        }),
      ]).start(!visible ? () => onHidden() : null);
    }
  }

  render() {
    const { children, height, style } = this.props;

    return (
      <Animated.View
        style={[
          {
            ...styles.container,
            height,
            transform: [{ translateY: this.animatedHeight }],
          },
          style,
        ]}
      >
        <Animated.View
          style={[styles.children, { opacity: this.animatedOpacity }]}
        >
          {children}
        </Animated.View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: colors.BLACK,
  },
  children: { width: '100%' },
});
