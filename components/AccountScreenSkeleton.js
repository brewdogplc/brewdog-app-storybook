import React, { Component } from 'react';
import {
  StyleSheet,
  StatusBar,
  ScrollView,
  View,
  Image,
  Text,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

import colors from '../constants/colors';
import fonts from '../constants/fonts';

import Button from './Button';
import Icon from './BrewDogIcon';

import AVATAR_PLACEHOLDER from '../assets/images/brewdog_shield_white.png';
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
  Shine,
} from 'rn-placeholder';

function SetStatusBarColor() {
  /*useFocusEffect(
    React.useCallback(() => {
      StatusBar.setBarStyle('light-content');
    }, [])
  );
*/
  return null;
}

//TODO turn in functional component
//TODO might not need it
export default class Account extends Component {
  handleScroll = (event) => {
    //TODO MAKE THIS ANIMATED
    if (event.nativeEvent.contentOffset.y > 0) {
      this.setHeaderOpacity(1);
    }
    if (event.nativeEvent.contentOffset.y == 0) {
      this.setHeaderOpacity(0);
    }
  };

  setHeaderOpacity = (opacity) => {
    const { navigation } = this.props;

    navigation.setOptions({
      headerTransparent: true,
      headerBackground: () => {
        return (
          <View
            style={[
              StyleSheet.absoluteFill,
              { backgroundColor: colors.BLACK, opacity: opacity },
            ]}
          />
        );
      },
    });
  };

  renderIdCards = () => {
    return [
      <Button
        key={1}
        title='My Equity Punk ID Card'
        icon={() => <Icon name='card' size={31} color={colors.WHITE} />}
        buttonStyle={styles.blackButton}
      />,
      <Button
        key={2}
        title='My Bond Holder Card'
        icon={() => <Icon name='card' size={31} color={colors.WHITE} />}
        buttonStyle={styles.blackButton}
      />,
    ];
  };

  renderInvestmentSections = () => {
    return [
      <View style={styles.section}>
        <Text style={styles.sectionTitle}>EQUITY FOR PUNKS UK</Text>
        <View style={styles.investmentCard}>
          <Placeholder Animation={Shine}>
            <PlaceholderLine
              width={'60%'}
              height={20}
              style={[{ marginTop: 23 }, { marginLeft: '20%' }]}
            />
          </Placeholder>
        </View>
      </View>,
      <View style={styles.section}>
        <Text style={styles.sectionTitle}>EQUITY FOR PUNKS USA</Text>
        <View style={styles.investmentCard}>
          <View style={styles.investmentDataContainer}>
            <Text style={styles.investmentDataLabel}>Shares</Text>
            <Text style={styles.investmentDataValue}>1000</Text>
          </View>
          <View style={styles.investmentDataContainer}>
            <Text style={styles.investmentDataLabel}>Bar Discount</Text>
            <Text style={styles.investmentDataValue}>10%</Text>
          </View>
          <View style={styles.investmentDataContainer}>
            <Text style={styles.investmentDataLabel}>Web Discount</Text>
            <Text style={styles.investmentDataValue}>20%</Text>
          </View>
        </View>
      </View>,
      <View style={styles.section}>
        <Text style={styles.sectionTitle}>BONDS</Text>
        <View style={styles.investmentCard}>
          <View style={styles.investmentDataContainer}>
            <Text style={styles.investmentDataLabel}>Bonds</Text>
            <Text style={styles.investmentDataValue}>5</Text>
          </View>
          <View style={styles.investmentDataContainer}>
            <Text style={styles.investmentDataLabel}>Bar Discount</Text>
            <Text style={styles.investmentDataValue}>10%</Text>
          </View>
          <View style={styles.investmentDataContainer}>
            <Text style={styles.investmentDataLabel}>Web Discount</Text>
            <Text style={styles.investmentDataValue}>20%</Text>
          </View>
        </View>
      </View>,
    ];
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle='light-content' />
        <SetStatusBarColor />
        <ScrollView
          bounces={false}
          contentContainerStyle={styles.scrollViewContent}
          scrollEventThrottle={16}
          onScroll={(event) => this.handleScroll(event)}
        >
          <View style={styles.headerContainer}>
            <Text style={styles.groupBadge}>{`STAFF`.toUpperCase()}</Text>
          </View>
          <View style={styles.avatarContainer}>
            <View style={styles.avatar}>
              <Image
                source={AVATAR_PLACEHOLDER}
                style={styles.avatarPlaceholder}
              />
            </View>
          </View>
          <View style={styles.accountCard}>
            <Placeholder Animation={Shine}>
              <PlaceholderMedia
                isRound={true}
                style={[
                  {
                    width: 125,
                    height: 125,
                    borderRadius: 125,
                    marginTop: -127,
                    marginRight: 'auto',
                    marginLeft: 'auto',
                  },
                ]}
              />
              <PlaceholderLine
                width={'60%'}
                height={20}
                style={[{ marginTop: 20 }, { marginLeft: '20%' }]}
              />
              <PlaceholderLine
                width={'80'}
                height={15}
                style={{ marginLeft: '10%' }}
              />
            </Placeholder>
            {/*<Text style={styles.username}>
              {`Ramsay Shiells`.toUpperCase()}
            </Text>
            <Text style={styles.joinedDate}>
              {`Joined January 2019`.toUpperCase()}
    </Text>*/}
            <View style={styles.section}>
              <Text style={styles.sectionTitle}>MY ID CARDS</Text>
              <Placeholder Animation={Shine}>
                <PlaceholderLine
                  width={'60%'}
                  height={20}
                  style={[
                    { marginTop: 23 },
                    { marginLeft: '20%' },
                    { backgroundColor: '#000' },
                  ]}
                />
              </Placeholder>
            </View>
            {this.renderInvestmentSections()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  scrollViewContent: {
    flexGrow: 1,
    width: '100%',
    alignItems: 'center',
    paddingBottom: 32,
  },
  headerContainer: {
    backgroundColor: colors.BOULDER,
    height: 220,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  groupBadge: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 16,
    lineHeight: 22,
    textAlign: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: colors.JAFFA,
  },
  avatarContainer: {
    backgroundColor: colors.WHITE,
    height: 130,
    width: 130,
    borderRadius: 130 / 2,
    position: 'absolute',
    top: 220 - 130 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    backgroundColor: colors.SILVER,
    height: 125,
    width: 125,
    borderRadius: 125 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarPlaceholder: { height: 76, resizeMode: 'contain' },
  accountCard: {
    flexGrow: 1,
    width: '100%',
    marginTop: 130 / 2,
    paddingHorizontal: 24,
    alignItems: 'center',
  },
  username: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 32,
    lineHeight: 32,
    paddingTop: 24,
  },
  joinedDate: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 18,
    lineHeight: 22,
    color: colors.BLACK,
    textAlign: 'center',
    paddingTop: 8,
  },
  section: { width: '100%', paddingTop: 32 },
  sectionTitle: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 18,
    lineHeight: 22,
    color: colors.BLACK,
    textAlign: 'center',
    paddingBottom: 8,
  },
  blackButton: { marginTop: 8 },
  investmentCard: {
    flexDirection: 'row',
    backgroundColor: colors.GALLERY,
    marginTop: 8,
    justifyContent: 'space-between',
  },
  investmentDataContainer: {
    alignItems: 'center',
    paddingVertical: 16,
    flex: 1,
  },
  investmentDataLabel: {
    fontFamily: fonts.NEUE_HAAS_GROTESK_DISPLAY_STD,
    fontSize: 14,
    lineHeight: 22,
  },
  investmentDataValue: {
    fontFamily: fonts.NEW_GROTESK_SQUARE,
    fontSize: 32,
    lineHeight: 32,
    paddingTop: 5,
  },
});
