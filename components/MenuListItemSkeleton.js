import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Placeholder, PlaceholderLine, Shine } from 'rn-placeholder';

import colors from '../constants/colors';

export default class MenuListItem extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Placeholder Animation={Shine}>
          <PlaceholderLine width={30} style={{ marginTop: 15 }} />
          <PlaceholderLine width={40} />
          <PlaceholderLine width={50} />
        </Placeholder>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.SILVER,
  },
});
