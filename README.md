To start run 'yarn start'


To run tests open index.js in the storybook folder uncomment these lines:


import registerRequireContextHook from 'babel-plugin-require-context-hook/register';
registerRequireContextHook();
const req = global.__requireContext(__dirname, '../components/stories', true, /.stories.js$/)

and comment out this line: 

const req = require.context('../components/stories/', true, /stories\.js$/)


Then run 'yarn test', to create new snapshots run 'yarn test --u'
