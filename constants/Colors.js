export default {
  BLACK: '#000000',
  WHITE: '#ffffff',
  SILVER: '#cccccc',
  BOULDER: '#7d7d7d',
  BREWDOG_BLUE: '#00AFDB',
  NOBEL: '#B3B3B3',
  UNTAPPD_YELLOW: '#ffcc00',
  LIMEADE: '#789904',
  GALLERY: '#ECECEC',
  JAFFA: '#F48B50'
};
